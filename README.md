# snack-services

Project to launch services with public APIs about all the modules.

## IPs per Server

|   Public IP    | Server Owner |  Ports available       |
| -------------- | ------------ | -----------------------|
| 13.58.62.62    |  Sergio      | 8080, 8099, 8888, 9002 |
| 18.191.230.112 |  Daniel      | 9003, 9006             |
| 18.222.31.138  |  Claudio     | 9001, 9007             |
|  3.21.241.173  |  Fabricio    | 9004, 9005             |

## Servicios

|  Service          |  Node    |   Port     |
| ----------------- | -------- | ---------- |
| users-service     | Claudio  | 9001 - On  |
| products-service  | Sergio   | 9002 - On  |
| orders-service    | Daniel   | 9003 - On  |
| company-service   | Fabricio | 9004 - On  |
| bff-customer      | Fabricio | 9005 - On  |
| bff-seller        | Daniel   | 9006 - On  |
| bff-dispatcher    | Claudio  | 9007 - On  |
| bff-adminsitrator |          | 9008 - off |
| config-server     | Sergio   | 8888 - on  |
| eureka-server     | Sergio   | 8099 - on  |
| API Gateway       | Sergio   | 8080 - on  |
