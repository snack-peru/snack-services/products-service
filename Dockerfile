FROM openjdk:8-alpine
LABEL maintainer="Sergio Rivas Medina -> sergiorm96@gmail.com"

ADD ./target/ /app
RUN mkdir -p /app/images/category/
EXPOSE 9002
WORKDIR /app
CMD java -jar products-backend*
