package com.snack.repositories;

import java.util.List;

import com.snack.entities.ProductCategory;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<ProductCategory, Integer> {
  
  @Query (value= "SELECT * FROM product_category ORDER BY ranking LIMIT 5", nativeQuery = true)
  public List<ProductCategory> getCategoriesByRanking();

}