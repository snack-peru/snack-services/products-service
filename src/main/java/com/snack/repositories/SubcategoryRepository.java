package com.snack.repositories;

import java.util.List;

import com.snack.entities.ProductSubCategory;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SubcategoryRepository extends JpaRepository<ProductSubCategory, Integer> {
    
    @Query(value = "select * from product_sub_category where id_product_category = ?1", nativeQuery = true)
    public List<ProductSubCategory> getSubcategoriesByCategory(Integer categoryId);
}