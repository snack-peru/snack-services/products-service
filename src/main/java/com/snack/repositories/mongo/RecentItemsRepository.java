package com.snack.repositories.mongo;

import com.snack.entities.mongo.RecentItems;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface RecentItemsRepository extends MongoRepository<RecentItems, Void>{
    
    RecentItems findByCustomerId(Long customerId);
}
