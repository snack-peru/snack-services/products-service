package com.snack.repositories.mongo;

import com.snack.entities.mongo.AproxPrice;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface AproxPriceRepository extends MongoRepository<AproxPrice, Long> {
    
    AproxPrice findByProductId(Long productId);
}