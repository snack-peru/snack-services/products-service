package com.snack.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.snack.entities.StoreXProduct;

import org.springframework.stereotype.Repository;

@Repository
public interface StoreXProductRepository extends JpaRepository<StoreXProduct, Long> {
    
    @Query(value = "select * from store_product where id_store = ?1", nativeQuery = true)
    List<StoreXProduct> getProductsByStore (Integer idStore);

    @Query(value = "select * from store_product where id_store = ?1 and id_product = ?2", nativeQuery = true)
    List<StoreXProduct> getProductsById (Integer idStore, Long idProduct);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM store_product WHERE id_storexproduct = ?1", nativeQuery = true)
    void deleteProduct(Long idStoreXProduct);

    List<StoreXProduct> findByIdStoreAndProduct_IdProductIn(Integer storeId, List<Long> productIdList);
}

