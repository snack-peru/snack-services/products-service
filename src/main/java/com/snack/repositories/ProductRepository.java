package com.snack.repositories;

import java.util.List;

import com.snack.entities.Product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    
    //Author: Fabricio; Method: 4 - Productdb
    @Query(value = "Select p.* from product p inner join product_sub_category sc on p.id_product_sub_category = sc.id_product_sub_category inner join product_category c on sc.id_product_category = c.id_product_category where c.id_product_category = ?1", nativeQuery = true)
    public List<Product> getProductsbyCategory (Integer categoryId);
    
    //Get all products of a subcategory
    @Query(value = "Select p.* from product p inner join product_sub_category sc on p.id_product_sub_category = sc.id_product_sub_category where sc.id_product_sub_category = ?1", nativeQuery = true)
    public List<Product> getProductsbySubcategory (Integer subcategoryId);

    //Author: Fabricio; Method: 6 - Productdb
    @Query(value = "SELECT p.* FROM product p INNER JOIN store_product sp on p.id_product = sp.id_product WHERE sp.id_store = ?1", nativeQuery = true)
    public List<Product> ProductsbyStore (Integer idstore);
    
    //Fabricio
    @Query(value = "select * from product where datediff(current_date(), creation_date) <= 5 ORDER BY creation_date desc;", nativeQuery=true)
    public List<Product> newproducts();
    
}

