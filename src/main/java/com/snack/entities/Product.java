package com.snack.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idProduct")
    private Long idProduct;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "packaging")
    private String packaging;

    @Column(name = "content")
    private String content;

    @Column(name = "image")
    private String image;
    
    @ManyToOne
    @JoinColumn(name = "idBrand")
    private Brand brand;  

    @Column(name = "visibility")
    private Integer visibility;

    @Column(name = "status")
    private Integer status;

    @Column(name = "creationDate")
    private Date creationDate;

    @Column(name = "ModificationDate")
    private Date ModificationDate;

    @ManyToOne
    @JoinColumn(name = "idUnitOfMeasurement")
    private UnitOfMeasurement unitOfMeasurement;

    @ManyToOne
    @JoinColumn(name = "idProductSubCategory")
    private ProductSubCategory productSubCategory;

    
}