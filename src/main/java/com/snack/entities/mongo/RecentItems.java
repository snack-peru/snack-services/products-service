package com.snack.entities.mongo;

import lombok.Builder;

import java.util.List;

import org.springframework.data.annotation.Id;

import com.snack.dto.RecentProductDTO;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Document(collection = "recent_items")
public class RecentItems {
    @Id
    private String recentItemsId;
    private Long customerId;
    private List<RecentProductDTO> lProducts;
}

