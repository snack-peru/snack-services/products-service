package com.snack.controllers;

import java.io.File;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import com.snack.dto.response.SubcategoryResponse;
import com.snack.entities.ProductCategory;
import com.snack.services.CategoryService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;

@RestController
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    private final String imageFile = "/images/category/";

    // Autor: Fabricio
    @GetMapping(value = "/api/categories")
    @ApiOperation(produces = MediaType.MULTIPART_FORM_DATA_VALUE, value = "Obtiene todas la categorias de productos",
            notes = "No requiere parámetros",
            responseContainer = "Object",
            response = ProductController.class)
    public List<ProductCategory> getAllProductCategories() {
        return categoryService.getAllProductCategories();
    }

    // Autor: Sergio
    @PostMapping(value = "/api/category/upload/image/{categoryId}")
    @ApiOperation(produces = MediaType.MULTIPART_FORM_DATA_VALUE, value = "Guarda la imagen de la categoria del producto",
            notes = "Se debe enviar como parámetro el id de la categoria e imagen como FORM DATA",
            responseContainer = "Object",
            response = ProductController.class)
    public ResponseEntity<?> uploadImage(@PathVariable(name = "categoryId") Integer categoryId,
            @RequestPart(name = "file") MultipartFile file) {
        // find category by id
        ProductCategory category = categoryService.findById(categoryId);

        if (category != null) {
            // set image field
            category.setImage(UUID.randomUUID().toString() + "-"
                    + file.getOriginalFilename().replace(" ", "").replace(":", "").replace("\\", ""));

            try {
                // save
                file.transferTo(new File(imageFile + category.getImage()));
                category = categoryService.saveCategory(category);
                return ResponseEntity.status(HttpStatus.CREATED).body(category);
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
            }
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No se encontró categoría");
    }

    // Author: Sergio Rivas
    @GetMapping(value = "/api/category/image/{name:.+}")
    @ApiOperation(produces = MediaType.MULTIPART_FORM_DATA_VALUE, value = "Obtiene la imagen de la categoria de productos",
            notes = "Se debe enviar como parámetro el nombre de la imagen",
            responseContainer = "Object",
            response = ProductController.class)
    public ResponseEntity<Resource> getImage(@PathVariable String name) throws MalformedURLException {
        Path path = Paths.get(imageFile).resolve(name).toAbsolutePath();

        Resource image = new UrlResource(path.toUri());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + image.getFilename() +"\"")
                .body(image);
    }

    @GetMapping(value = "/api/categoriesByRanking")
    public List<ProductCategory> getCategoriesByRanking(){
        return categoryService.getCategoriesByRanking();
    }

    //Get all subcategories with products of a category
    @GetMapping(value = "/api/category/{categoryId}/subcategories")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE,
                    value = "Obtiene todos las subcategorias con sus productos correspondiente a la categoria solicitada",
                    notes = "Se requiere el ID Category",
                    responseContainer = "Object")
    public List<SubcategoryResponse> getSubcategoriesWithProductsByCategory(@PathVariable("categoryId") Integer categoryId){
        return categoryService.getSubcategoriesWithProductsByCategory(categoryId);
    }
}