package com.snack.controllers;

import java.util.Collections;
// import java.util.HashMap;
import java.util.List;

import com.snack.dto.StoreXProductDTO;
import com.snack.dto.request.StoreAndProductsRequest;
import com.snack.dto.request.StoreXProductRequest;
import com.snack.dto.request.StoreXProductUpdateRequest;
import com.snack.entities.StoreXProduct;
import com.snack.services.StoreXProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;

@RestController
@CrossOrigin
@RequestMapping("/api/storexproduct")
public class StoreXProductController {
    
    @Autowired
    private StoreXProductService storeXProductService;

    @GetMapping(value = "/store/{idStore}")
    @ApiOperation(produces = MediaType.MULTIPART_FORM_DATA_VALUE, value = "Obtiene todos los productos de la bodega",
            notes = "Se debe enviar como parametro el ID de la bodega",
            responseContainer = "Object",
            response = ProductController.class)
    public List<StoreXProduct> getProductsByStore(@PathVariable("idStore") Integer idStore) {
        return storeXProductService.getProductsByStore(idStore);
    }

    @GetMapping(value = "/store/{idStore}/products")
    @ApiOperation(produces = MediaType.MULTIPART_FORM_DATA_VALUE, value = "Obtiene todos los productos de la bodega",
            notes = "Se debe enviar como parametro el ID de la bodega",
            responseContainer = "Object",
            response = ProductController.class)
    public List<StoreXProductDTO> getProductsByIdStore(@PathVariable("idStore") Integer idStore) {
        return storeXProductService.getProductsByIdStore(idStore);
    }

    @PostMapping(value = "/store/product/add")
    @ApiOperation(produces = MediaType.MULTIPART_FORM_DATA_VALUE, value = "Registra un nuevo producto para la bodega",
            notes = "Se debe enviar como parametro el objeto StoreXProduct",
            responseContainer = "Object",
            response = ProductController.class)
    public ResponseEntity<?> addProduct(@RequestBody StoreXProductRequest request) {
        try {
            StoreXProduct response = storeXProductService.addProduct(request);
            return new ResponseEntity<StoreXProduct>(response, HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
        }
    }

    @PostMapping(value = "/store/product/update")
    @ApiOperation(produces = MediaType.MULTIPART_FORM_DATA_VALUE, value = "Actualiza los datos del registro del producto de la bodega",
            notes = "Se debe enviar como parametro el objeto StoreXProduct (con idStoreXProduct)",
            responseContainer = "Object",
            response = ProductController.class)
    public ResponseEntity<?> updateProduct(@RequestBody StoreXProductUpdateRequest request) {
        try {
            StoreXProduct response = storeXProductService.updateProduct(request);
            return new ResponseEntity<StoreXProduct>(response, HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
        }
    }

    @PostMapping(value = "/store/product/delete/{idStoreXProduct}")
    @ApiOperation(produces = MediaType.MULTIPART_FORM_DATA_VALUE, value = "Elimina el registro del producto de la bodega",
            notes = "Se debe enviar como parametro el ID de StoreXProduct",
            responseContainer = "Object",
            response = ProductController.class)
    public void deleteProduct(@PathVariable("idStoreXProduct") Long idStoreXProduct) {
        storeXProductService.deleteProduct(idStoreXProduct);
    }

    @PostMapping(value="/store/products/realPrice")
    @ApiOperation(produces = MediaType.MULTIPART_FORM_DATA_VALUE, value = "Obtiene los precios reales de una lista de productos",
            notes = "Se debe enviar como parámetros la lista de ids de productos y el id de la tienda",
            responseContainer = "Object",
            response = ProductController.class)
    public ResponseEntity<?> getRealPriceByListOfProducts( @RequestBody List<StoreAndProductsRequest> storeAndProducts){
        try{
            List<StoreAndProductsRequest> storeAndProductsResponse = storeXProductService.getRealPriceByListOfProducts(storeAndProducts);
            return ResponseEntity.status(HttpStatus.OK).body(storeAndProductsResponse);
        } catch(HttpClientErrorException e){
            return ResponseEntity.status(e.getStatusCode()).body(Collections.singletonMap("message", e.getResponseBodyAsString()));
        }
    }
}


