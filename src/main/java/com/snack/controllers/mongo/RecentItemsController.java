package com.snack.controllers.mongo;

// import io.swagger.annotations.ApiOperation;

import com.snack.dto.RecentProductDTO;
import com.snack.entities.mongo.RecentItems;
import com.snack.services.mongo.RecentItemsService;

// import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
// import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;


@RestController
@CrossOrigin
@RequestMapping(value = "/api/recentItems")
public class RecentItemsController {
    
    @Autowired
    RecentItemsService recentItemsService;

    //Obtener productos recientes por cliente
    @GetMapping(value="/{customerId}")
    public RecentItems getRecentItemsbyCustomerId(@PathVariable("customerId") Long customerId) {
        // return recentItemsService.getRecentItemsByCustomerId(customerId);
        RecentItems pCustomer;
        pCustomer = recentItemsService.getRecentItemsByCustomerId(customerId);
        return pCustomer;
    }

    @PostMapping(value="/{customerId}")
    public void newRecentItem(@PathVariable(name = "customerId") Long customerId, @RequestBody RecentProductDTO recentProductDTO) {
        RecentItems productRecentItems = recentItemsService.getRecentItemsByCustomerId(customerId);
        
        if(productRecentItems == null){
            recentItemsService.saveNewRecentItems(customerId, recentProductDTO);
        } else {
            recentItemsService.addProducttoRecentItems(customerId, recentProductDTO);
        }

    }

    @PutMapping(value="/{customerId}")
    public void deleteProducttoRecentItems(@PathVariable (name = "customerId") Long customerId, @RequestBody RecentProductDTO recentProductDTO) {
        recentItemsService.deleteProductRecentItems(customerId, recentProductDTO);
    } 
    
    
}
