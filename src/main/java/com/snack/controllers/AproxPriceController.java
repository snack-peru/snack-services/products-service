package com.snack.controllers;

import java.util.Collections;
import java.util.List;

import com.snack.dto.AproxPriceDTO;
import com.snack.entities.mongo.AproxPrice;
import com.snack.services.AproxPriceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.http.MediaType;
import io.swagger.annotations.ApiOperation;

@RestController
public class AproxPriceController {
    
    @Autowired
    AproxPriceService aproxPriceService;

    @PostMapping(value="/api/aproxPrice")
    @ApiOperation(produces = MediaType.MULTIPART_FORM_DATA_VALUE, value = "Guarda el precio aproximado de un producto",
            notes = "Se debe enviar como parámetro el id del producto y el precio aproximado",
            responseContainer = "Object",
            response = ProductController.class)
    public ResponseEntity<?> saveAproxPrice(@RequestBody AproxPriceDTO aproxPriceDto){
        //AproxPrice aproxPrice = new AproxPrice();
        try{
            AproxPrice aproxPrice=aproxPriceService.saveAproxPrice(aproxPriceDto);
            return new ResponseEntity<AproxPrice>(aproxPrice, HttpStatus.CREATED);
        } catch(HttpClientErrorException e){
            return ResponseEntity.status(e.getStatusCode()).body(Collections.singletonMap("message", e.getResponseBodyAsString()));
        }
    }

    @GetMapping(value="/api/aproxPrice/{productId}")
    @ApiOperation(produces = MediaType.MULTIPART_FORM_DATA_VALUE, value = "Obtiene el precio aproximado de un producto",
            notes = "Se debe enviar como parámetro el id del producto",
            responseContainer = "Object",
            response = ProductController.class)
    public ResponseEntity<?> getAproxPriceByProductId(@PathVariable Long productId){
        //AproxPrice aproxPrice = new AproxPrice();
        try{
            AproxPrice aproxPrice=aproxPriceService.getAproxPrice(productId);
            return new ResponseEntity<AproxPrice>(aproxPrice, HttpStatus.CREATED);
        } catch(HttpClientErrorException e){
            return ResponseEntity.status(e.getStatusCode()).body(Collections.singletonMap("message", e.getResponseBodyAsString()));
        }
    }

    @PostMapping(value="/api/ListAproxPrice/")
    @ApiOperation(produces = MediaType.MULTIPART_FORM_DATA_VALUE, value = "Obtiene los precio aproximados de una lista de productos",
            notes = "Se debe enviar como parámetro la lista de ids de productos",
            responseContainer = "Object",
            response = ProductController.class)
    public ResponseEntity<?> getAproxPriceByListOfProducts(@RequestBody List<AproxPriceDTO> productIdList){
        try{
            List<AproxPrice> aproxPrice=aproxPriceService.getAproxPriceByListOfProducts(productIdList);
            return new ResponseEntity <List<AproxPrice>>(aproxPrice, HttpStatus.CREATED);
        } catch(HttpClientErrorException e){
            return ResponseEntity.status(e.getStatusCode()).body(Collections.singletonMap("message", e.getResponseBodyAsString()));
        }
    }
}