package com.snack.controllers;

import java.util.List;

import com.snack.entities.ProductSubCategory;
import com.snack.services.SubcategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;

@RestController
public class SubCategoryController {
    
    @Autowired
    private SubcategoryService subCategoryService;

    @GetMapping(value = "/api/subcategory/{categoryId}")
    @ApiOperation(produces = MediaType.MULTIPART_FORM_DATA_VALUE, value = "Obtiene todos las subcategorias de una categoria",
            notes = "Se debe enviar como parámetro el id de la categoria",
            responseContainer = "Object",
            response = ProductController.class)
    public List<ProductSubCategory> getSubcategoriesByCategory(@PathVariable("categoryId") Integer categoryId) {
        return subCategoryService.getSubcategoriesByCategory(categoryId);
    } 
}