package com.snack.controllers;

import java.io.File;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import com.snack.entities.Product;
import com.snack.services.ProductService;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


@RestController
@CrossOrigin
public class ProductController {

    @Autowired
    private ProductService productService;

    private final String imageFile = "/images/products/";

    //By: Fabricio
    @GetMapping(value = "/api/productsbycategory/{categoryId}")
    @ApiOperation(produces = MediaType.MULTIPART_FORM_DATA_VALUE, value = "Obtiene todos los productos de una categoria",
            notes = "Se debe enviar como parametro el id de la categoria",
            responseContainer = "Object",
            response = ProductController.class)
    public List<Product> getProductsByCategory (@PathVariable("categoryId") Integer categoryId) {
        return productService.getProductsbyCategory(categoryId);
    }

    //by: Fabricio
    @GetMapping(value = "/api/product/{idProduct}")
    @ApiOperation(produces = MediaType.MULTIPART_FORM_DATA_VALUE, value = "Obtiene el detalle del producto",
            notes = "Se debe enviar como parametro el id del producto",
            responseContainer = "Object",
            response = ProductController.class)
    public Product getProductsById (@PathVariable("idProduct") Long idProduct) {
        return productService.getProductById(idProduct);
    }
    
    //Author: Fabricio; Method: 6 - Productdb
    @GetMapping(value = "/api/productByStore/{idstore}")
    @ApiOperation(produces = MediaType.MULTIPART_FORM_DATA_VALUE, value = "Obtiene todos los productos de la bodega",
            notes = "Se debe enviar como parametro el id de la bodega",
            responseContainer = "Object",
            response = ProductController.class)
    public List<Product> getProductsByStore (@PathVariable("idstore") Integer idstore) {
        return productService.getProductsbyStore(idstore);
    }

    //Fabricio
    @GetMapping(value = "/api/product/newproducts")
    @ApiOperation(produces = MediaType.MULTIPART_FORM_DATA_VALUE, value = "Obtiene todos los productos registrados en los últimos 5 días",
            notes = "No require parámetros",
            responseContainer = "Object",
            response = ProductController.class)
    public List<Product> getNewProducts() {
        return productService.getNewProducts();
    }

    @GetMapping(value = "/api/product/search/{searchkey}")
    @ApiOperation(produces = MediaType.MULTIPART_FORM_DATA_VALUE, value = "Obtiene todos los productos según el texto buscado",
            notes = "Se debe enviar como parametro el texto a buscar. Hace la coincidencia en base al nombre del producto",
            responseContainer = "Object",
            response = ProductController.class)
    public List<Product> findProducts(@PathVariable("searchkey") String searchkey){
        return productService.findProductByName(searchkey);
    }

    // Autor: Sergio
    @PostMapping(value = "/api/product/upload/image/{productId}")
    @ApiOperation(produces = MediaType.MULTIPART_FORM_DATA_VALUE, value = "Carga la imagen del producto al servidor",
            notes = "Se debe enviar como parametro el id de producto y la imagen como FORM-DATA",
            responseContainer = "Object",
            response = ProductController.class)
    public ResponseEntity<?> uploadImage(@PathVariable(name = "productId") Long categoryId,
                                         @RequestPart(name = "file") MultipartFile file) {
        // find product by id
        Product product = productService.findById(categoryId);

        if (product != null) {
            // set image field
            product.setImage(UUID.randomUUID().toString() + "-"
                    + file.getOriginalFilename().replace(" ", "").replace(":", "").replace("\\", ""));

            try {
                // save
                file.transferTo(new File(imageFile + product.getImage()));
                product = productService.saveCategory(product);
                return ResponseEntity.status(HttpStatus.CREATED).body(product);
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
            }
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No se encontró id de producto");
    }

    //Author: Daniel
    @GetMapping(value = "/api/product/image/{name:.+}")
    @ApiOperation(produces = MediaType.MULTIPART_FORM_DATA_VALUE, value = "Obtiene la imagen del producto del servidor",
            notes = "Se debe enviar como parametro el nombre de la imagen",
            responseContainer = "Object",
            response = ProductController.class)
    public ResponseEntity<Resource> getImage (@PathVariable String name) throws MalformedURLException {
        Path path = Paths.get(imageFile).resolve(name).toAbsolutePath();

        Resource image = new UrlResource(path.toUri());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+ image.getFilename()+"\"")
                .body(image);
    }

}