package com.snack.dto.response;

import com.snack.dto.SubcategoryDTO;
import com.snack.dto.ProductDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubcategoryResponse {
    private SubcategoryDTO subcategory;
    private List<ProductDTO> products;
}