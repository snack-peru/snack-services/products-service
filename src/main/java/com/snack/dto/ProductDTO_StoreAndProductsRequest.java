package com.snack.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductDTO_StoreAndProductsRequest {
    private Long productId;
    private String brand;
    private String name;
    private Integer quantity;
    private String measurement;
    private Double price;
}
