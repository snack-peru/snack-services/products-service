package com.snack.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class AproxPriceDTO {
private Long productId;
    private Double aproxPrice;
    private Integer quantity;
}