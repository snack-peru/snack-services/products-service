package com.snack.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StoreXProductDTO {
    private Long idStoreXProduct;
    private Double price;
    private Integer idStore;
    private ProductDTO product;
}