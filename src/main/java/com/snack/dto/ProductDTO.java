package com.snack.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.snack.entities.Brand;
import com.snack.entities.UnitOfMeasurement;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class ProductDTO {
    private Long idProduct;
    private String name;
    private String image;
    private Brand brand;
    private UnitOfMeasurement unitOfMeasurement;
}