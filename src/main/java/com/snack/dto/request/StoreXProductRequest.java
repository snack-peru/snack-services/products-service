package com.snack.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StoreXProductRequest {

    private Double price;
    private Integer status;
    private Integer idStore;
    private Long idProduct;

}