package com.snack.dto.request;

import java.util.List;

import com.snack.dto.ProductDTO_StoreAndProductsRequest;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class StoreAndProductsRequest {
    private Integer storeId;
    private String name;
    private String deliverytype;
    private List<ProductDTO_StoreAndProductsRequest> products;
}
