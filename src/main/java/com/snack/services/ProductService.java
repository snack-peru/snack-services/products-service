package com.snack.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.snack.entities.Product;
import com.snack.repositories.ProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {
    
    @Autowired
    private ProductRepository productRepository;

    //Sergio
    public Product findById(Long id) {
        Optional<Product> optional = productRepository.findById(id);
        if (optional.isPresent()) return optional.get();
        return null;
    }

    //Sergio
    public Product saveCategory(Product product) {
        return productRepository.save(product);
    }

    /*
    listar productos por categoria
    By: Fabricio
    */
    public List<Product> getProductsbyCategory (Integer categoryId) {
        return productRepository.getProductsbyCategory(categoryId);
    }

    //Get all products of a subcategory
    public List<Product> getProductsbySubcategory (Integer subcategoryId) {
        return productRepository.getProductsbySubcategory(subcategoryId);
    }
    
    /*
    listar productos por Id
    By: Fabricio
    */
    public Product getProductById (Long idProduct) {
        Optional<Product> optional = productRepository.findById(idProduct);
        if(optional.isPresent()){
            Product product = optional.get();
            return product;
        }
        return null;
    }

    //Author: Fabricio; Method: 6 - Productdb 
    public List<Product> getProductsbyStore (Integer idstore){
        return productRepository.ProductsbyStore(idstore);
    }

    //Fabricio
    public List<Product> getNewProducts(){
        return productRepository.newproducts();
    }

    public List<Product> findProductByName(String chain) {
       List<Product> Products = productRepository.findAll();
        //Inicializo lista e ingreso los productos
        List<String> SearchKeys = new ArrayList<String>();
        if (!chain.isEmpty()){
            SearchKeys = Arrays.asList((chain.toLowerCase()).split(" "));
        }
        if (SearchKeys.size() > 0){
            List<Product> result = new ArrayList<Product>();

            for(Product a : Products){
                List<String> productname = new ArrayList<String>();
                productname.addAll(Arrays.asList((a.getName().toLowerCase()).split(" ")));
                if(evaluteSearch(productname, SearchKeys)){
                    result.add(a);
                }
            }
            return result;
        }
        else {
            return Products;
        }

    }

    private Boolean evaluteSearch(List<String> values, List<String> searchKeys){
        List<Integer> booleans = new ArrayList<Integer>();
        Integer temp;

        for(String s: searchKeys){
            temp = 0;
            for(String v: values){
                if(v.contains(s)) 
                temp += 1;

            }
            if(temp>0)
                booleans.add(1);
            else
                booleans.add(0);
        }

            Integer result = 1;
            for(Integer i: booleans)
                result *= i;
        
        return result>0? true : false;
    }

}