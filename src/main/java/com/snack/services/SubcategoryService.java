package com.snack.services;

import java.util.List;

import com.snack.entities.ProductSubCategory;
import com.snack.repositories.SubcategoryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubcategoryService {
    
    @Autowired
    private SubcategoryRepository subcategoryRepository;

    public List<ProductSubCategory> getSubcategoriesByCategory(Integer categoryId) {
        return subcategoryRepository.getSubcategoriesByCategory(categoryId);
    }
}