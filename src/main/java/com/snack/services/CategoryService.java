package com.snack.services;

import java.util.ArrayList;
import java.util.List;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snack.repositories.CategoryRepository;

import com.snack.entities.ProductCategory;
import com.snack.entities.ProductSubCategory;
import com.snack.entities.Product;

import com.snack.dto.response.SubcategoryResponse;
import com.snack.dto.SubcategoryDTO;
import com.snack.dto.ProductDTO;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepository CategoryRepository;
    @Autowired
    private SubcategoryService subcategoryService;
    @Autowired
    private ProductService productService;
    
    //Autor: Fabricio
    public List<ProductCategory> getAllProductCategories() {
        return CategoryRepository.findAll();
    }

    //Sergio
    public ProductCategory findById(Integer id) {
        Optional<ProductCategory> optional = CategoryRepository.findById(id);
        if (optional.isPresent()) return optional.get();
        return null;
    }

    //Sergio
    public ProductCategory saveCategory(ProductCategory category) {
        return CategoryRepository.save(category);
    }
    
    public List<ProductCategory> getCategoriesByRanking() {
        return CategoryRepository.getCategoriesByRanking();
    }

    public List<SubcategoryResponse> getSubcategoriesWithProductsByCategory(Integer categoryId) {
        List<SubcategoryResponse> lsscres = new ArrayList<SubcategoryResponse>();
        for(ProductSubCategory s: subcategoryService.getSubcategoriesByCategory(categoryId)){
            SubcategoryResponse scres = new SubcategoryResponse();
            SubcategoryDTO scdto = new SubcategoryDTO();
            scdto.setIdProductSubCategory(s.getIdProductSubCategory());
            scdto.setName(s.getName());
            List<ProductDTO> lspdto = new ArrayList<ProductDTO>();;
            for(Product p: productService.getProductsbySubcategory(s.getIdProductSubCategory())){
                ProductDTO pdto = new ProductDTO();
                pdto.setIdProduct(p.getIdProduct());
                pdto.setName(p.getName());
                pdto.setImage(p.getImage());
                pdto.setBrand(p.getBrand());
                pdto.setUnitOfMeasurement(p.getUnitOfMeasurement());
                lspdto.add(pdto);
            }
            scres.setSubcategory(scdto);
            scres.setProducts(lspdto);
            lsscres.add(scres);
        }
        return lsscres;
    }
}