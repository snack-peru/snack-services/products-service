package com.snack.services.mongo;

// import java.util.LinkedList;
// import java.util.Queue;

import java.util.*;

import com.snack.dto.RecentProductDTO;
import com.snack.entities.mongo.RecentItems;
import com.snack.repositories.mongo.RecentItemsRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RecentItemsService {
    @Autowired
    RecentItemsRepository recentItemsRepository;

    public RecentItems getRecentItemsByCustomerId(Long customerId) {
        RecentItems recentCustomer = recentItemsRepository.findByCustomerId(customerId);
        return recentCustomer;
    }

    public void saveNewRecentItems(Long customerId, RecentProductDTO recentProductDTO) {
        RecentItems recentItems;
        RecentProductDTO recentProduct = RecentProductDTO.builder().idProduct(recentProductDTO.getIdProduct())
                .productName(recentProductDTO.getProductName()).build();

        // Queue<RecentProductDTO> queueProducts = new LinkedList<>();
        List<RecentProductDTO> listProducts = new ArrayList<RecentProductDTO>();
        listProducts.add(recentProduct);

        recentItems = RecentItems.builder().customerId(customerId).lProducts(listProducts).build();

        recentItemsRepository.save(recentItems);
    }

    public void addProducttoRecentItems(Long customerId, RecentProductDTO recentProductDTO) {
        RecentItems productsCustomer = recentItemsRepository.findByCustomerId(customerId);

        RecentProductDTO recentProduct = RecentProductDTO.builder().idProduct(recentProductDTO.getIdProduct()).productName(recentProductDTO.getProductName()).build();

        List<RecentProductDTO> listProducts = productsCustomer.getLProducts();
        Queue<RecentProductDTO> qCustomer = new LinkedList<>(listProducts);
        Long productId = recentProductDTO.getIdProduct();

        if (qCustomer != null) {
            //si trae la lista de productos
            int size = qCustomer.size();
            int exist = 0;
            
            if(size >= 10){qCustomer.remove();}

            for (RecentProductDTO rp: qCustomer){
                Long qProductId = rp.getIdProduct();
                if(qProductId == productId) {
                    exist = 1;
                    break;
                }
            }

            if(exist == 0){qCustomer.add(recentProduct);}
        }

        List<RecentProductDTO> aux = new ArrayList<RecentProductDTO>(qCustomer);

        productsCustomer.setLProducts(aux);
        recentItemsRepository.save(productsCustomer);
    }

    
    public void deleteProductRecentItems(Long customerId, RecentProductDTO recentProductDTO){
        RecentItems productsCustomer = recentItemsRepository.findByCustomerId(customerId);

        List<RecentProductDTO> listProducts = productsCustomer.getLProducts();
        Long productId = recentProductDTO.getIdProduct();
        
        RecentProductDTO aux = new RecentProductDTO();

        for (RecentProductDTO rp: listProducts){
            Long qProductId = rp.getIdProduct();

            if(qProductId == productId){
                aux = rp;
            }
        }

        listProducts.remove(aux);

        productsCustomer.setLProducts(listProducts);
        recentItemsRepository.save(productsCustomer);
    }
    
}
