package com.snack.services;

import java.util.ArrayList;
import java.util.List;

import com.snack.dto.AproxPriceDTO;
import com.snack.entities.mongo.AproxPrice;
import com.snack.repositories.mongo.AproxPriceRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AproxPriceService {
    @Autowired
    AproxPriceRepository aproxPriceRepository;

    public AproxPrice saveAproxPrice (AproxPriceDTO aproxPriceDto){
        AproxPrice aproxPrice = new AproxPrice();
        aproxPrice.setAproxPrice(aproxPriceDto.getAproxPrice());
        aproxPrice.setProductId(aproxPriceDto.getProductId());
        return aproxPriceRepository.save(aproxPrice);
    }

    public AproxPrice getAproxPrice (Long productId){

        return aproxPriceRepository.findByProductId(productId);
        //Optional<AproxPrice> optional = aproxPriceRepository.findByProductId(productId);
        //if (optional.isPresent()) return optional.get();
        //return null;
    }

    public List<AproxPrice> getAproxPriceByListOfProducts (List<AproxPriceDTO> productIdList){
        List<AproxPrice> listAproxPrice = new ArrayList<AproxPrice>();
        for(AproxPriceDTO productId : productIdList){
            listAproxPrice.add(aproxPriceRepository.findByProductId(productId.getProductId()));
        }
        return listAproxPrice;

    }

}