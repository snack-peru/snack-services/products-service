package com.snack.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import com.snack.dto.ProductDTO;
import com.snack.dto.ProductDTO_StoreAndProductsRequest;
import com.snack.dto.StoreXProductDTO;
import com.snack.dto.request.StoreAndProductsRequest;
import com.snack.dto.request.StoreXProductRequest;
import com.snack.dto.request.StoreXProductUpdateRequest;
import com.snack.entities.Product;
import com.snack.entities.StoreXProduct;
import com.snack.repositories.StoreXProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StoreXProductService {
    
    @Autowired
    private StoreXProductRepository storeXProductRepository;

    public List<StoreXProduct> getProductsByStore(Integer idStore) {
        return storeXProductRepository.getProductsByStore(idStore);
    }

    public List<StoreXProductDTO> getProductsByIdStore(Integer idStore) {
        List<StoreXProductDTO> lsStoreXProductDTO = new ArrayList<StoreXProductDTO>();
        for(StoreXProduct storexproduct: storeXProductRepository.getProductsByStore(idStore)){
            lsStoreXProductDTO.add(StoreXProductDTO.builder()
                .idStoreXProduct(storexproduct.getIdStoreXProduct())
                .idStore(storexproduct.getIdStore())
                .price(storexproduct.getPrice())
                .product(ProductDTO.builder()
                        .idProduct(storexproduct.getProduct().getIdProduct())
                        .name(storexproduct.getProduct().getName())
                        .image(storexproduct.getProduct().getImage())
                        .brand(storexproduct.getProduct().getBrand())
                        .unitOfMeasurement(storexproduct.getProduct().getUnitOfMeasurement())
                        .build()
                )
                .build());
        }

        return lsStoreXProductDTO;
    }

    public StoreXProduct addProduct(StoreXProductRequest request) throws Exception {
        if(storeXProductRepository.getProductsById(request.getIdStore(), request.getIdProduct()).size() == 0) {
            return storeXProductRepository.save(
                StoreXProduct.builder()
                .idStore(request.getIdStore())
                .price(request.getPrice())
                .status(request.getStatus())
                .product(Product.builder().idProduct(request.getIdProduct()).build())
                .build()
            );
        }

        throw new Exception("Ya agregó el producto elegido");
    }

    public StoreXProduct updateProduct(StoreXProductUpdateRequest request) {
        return storeXProductRepository.save(
            StoreXProduct.builder()
            .idStoreXProduct(request.getIdStoreXProduct())
            .idStore(request.getIdStore())
            .price(request.getPrice())
            .status(request.getStatus())
            .product(Product.builder().idProduct(request.getIdProduct()).build())
            .build()
        );
    }

    public void deleteProduct(Long idStoreXProduct) {
        storeXProductRepository.deleteProduct(idStoreXProduct);
    }

    public List<StoreAndProductsRequest> getRealPriceByListOfProducts(List<StoreAndProductsRequest> storeAndProductsRequest) {
            
        for(StoreAndProductsRequest storeAndProducts : storeAndProductsRequest){
            List<Long> idProducts = new ArrayList<Long>();
            for(ProductDTO_StoreAndProductsRequest productDto: storeAndProducts.getProducts()){
                idProducts.add(productDto.getProductId());
            }
            List<StoreXProduct> responseList = storeXProductRepository.findByIdStoreAndProduct_IdProductIn(storeAndProducts.getStoreId(), idProducts);
            HashMap<Long, Double> responseMap = new HashMap<Long,Double>();
            for (StoreXProduct storeXProduct : responseList) {
                responseMap.put(storeXProduct.getProduct().getIdProduct(), storeXProduct.getPrice());
            }
            for(ProductDTO_StoreAndProductsRequest productDto: storeAndProducts.getProducts()){
                productDto.setPrice(responseMap.get(productDto.getProductId()));
            }
        }
        return storeAndProductsRequest;
    }
}